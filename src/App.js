import logo from "./logo.svg";
import "./App.css";
import InterfaceAPI from "./interface";

function App() {
  return (
    <div className="App">
      <InterfaceAPI />
    </div>
  );
}

export default App;
